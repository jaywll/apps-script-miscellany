function doGet(e) {
  return HtmlService.createTemplateFromFile('Index').evaluate().setTitle("Lunch Deciderer™");
}

function getJSONData() {
  var spreadsheet = SpreadsheetApp.openById('1gTOzek6wmNtGG6AzNKNzLYo8oXh_aStQ4CLsIx-OwJs');
  var sheet = spreadsheet.getSheets()[0];
  var data = sheet.getDataRange().getValues();
  
  var people = [];
  var places = [];
  
  for (var i = 1; i < data.length; i++) {
    if (data[i][0]) people.push(data[i][0]);
    if (data[i][1]) places.push(data[i][1]);
  }
  
  var outputobj = {people:people, places:places};
  return JSON.stringify(outputobj);
}