Welcome to my Apps Script Miscellany repository!
================================================

This is a place where I make available snippets of apps script that just may be
useful to someone. Single-file apps (which I expect is mostly what will live here)
are in [snippets](https://gitlab.com/jaywll/apps-script-miscellany/snippets). Apps
that are split over multiple files are organized into folders right within this
main repository.

What's here?
------------
*  [Sheets reference fixer](https://gitlab.com/jaywll/apps-script-miscellany/snippets/1691618)
*  [Lunch Deciderer](https://gitlab.com/jaywll/apps-script-miscellany/tree/master/Lunch%20Deciderer)